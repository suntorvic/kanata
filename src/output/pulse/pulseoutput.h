#ifndef PULSEOUTPUT_H
#define PULSEOUTPUT_H

#include <kanata_plugin.h>

void* kanata_plugin_init ( struct KanataContext* kntctx );
void* kanata_plugin_close ( struct KanataContext* kntctx, void* userdata );

#include <pulse/pulseaudio.h>
#include <pulse/error.h>

typedef struct KntPulseOutput {
    pa_threaded_mainloop* paMainLoop;
    pa_mainloop_api* paMainLoopApi;
    pa_stream* paStream;
    pa_context* paContext;
    int err;
} KntPulseOutput;

KanataOutput kntOutputObject;

void knt_pulseout_initobject( void** userdata );

void knt_pulseout_freeobject( void** userdata );

void knt_pulseout_write( KanataBuffer* buffer, void** userdata);

void knt_pulseout_flush( void** userdata );

KntPulseOutput* pulseout_new();

void pulseout_free ( KntPulseOutput* kntOutput );

void pulseout_playbuffer ( KntPulseOutput* kntOutput, KanataBuffer* kanataBuffer );

void pulseout_contextnotifycallback ( pa_context* context, void* userdata );

void pulseout_contextstatecallback ( pa_context* context, void* userdata );

void pulseout_streamstatecallback ( pa_stream* p, void* userdata );

void pulseout_streamwritecallback ( pa_stream* p, size_t nbytes, void* userdata );

void pulseout_flush ( KntPulseOutput* kntOutput );

static const char* kntPulseClientName = "Kanata";

#endif // PULSEOUTPUT_H
