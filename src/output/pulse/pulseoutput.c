#include <stdio.h>
#include <stdlib.h>
#include "pulseoutput.h"

void* kanata_plugin_init ( struct KanataContext* kntctx )
{
    kntOutputObject.init = knt_pulseout_initobject;
    kntOutputObject.close = knt_pulseout_freeobject;
    kntOutputObject.write = knt_pulseout_write;
    kntOutputObject.flush = knt_pulseout_flush;
    kanata_register_output ( kntctx, &kntOutputObject );
    return NULL;
}

void* kanata_plugin_close ( struct KanataContext* kntctx, void* userdata )
{
    KntPulseOutput* pulseOutput = ( KntPulseOutput* ) userdata;
}

static pa_sample_spec ss = {
    .format = PA_SAMPLE_S16LE,
    .rate = 44100,
    .channels = 2
};

void knt_pulseout_initobject ( void** userdata )
{
    ( *userdata ) = pulseout_new();
}

void knt_pulseout_freeobject ( void** userdata )
{
    pulseout_free ( *userdata );
}

void knt_pulseout_write ( KanataBuffer* buffer, void** userdata )
{
    pulseout_playbuffer ( *userdata, buffer );
}

void knt_pulseout_flush ( void** userdata )
{
    pulseout_flush ( *userdata );
}

KntPulseOutput* pulseout_new()
{
    KntPulseOutput* kntOutput = ( KntPulseOutput* ) malloc ( sizeof ( KntPulseOutput ) );
    kntOutput->paMainLoop = pa_threaded_mainloop_new();
    kntOutput->paMainLoopApi = pa_threaded_mainloop_get_api ( kntOutput->paMainLoop );
    int r = pa_signal_init ( kntOutput->paMainLoopApi );

    if ( ! ( kntOutput->paContext = pa_context_new ( kntOutput->paMainLoopApi, kntPulseClientName ) ) ) {
        kanata_error ( "pa_context_new() failed.\n" );
    }

    pa_context_set_state_callback ( kntOutput->paContext, pulseout_contextstatecallback, kntOutput );

    // Connect the context
    if ( ( pa_context_connect ( kntOutput->paContext, NULL, PA_CONTEXT_NOFLAGS, NULL ) ) < 0 ) {
        kanata_error ( "pa_context_connect failed: %s\n", pa_strerror ( pa_context_errno ( kntOutput->paContext ) ) );
    }

    pa_threaded_mainloop_lock ( kntOutput->paMainLoop );

    if ( pa_threaded_mainloop_start ( kntOutput->paMainLoop ) < 0 ) {
        kanata_error ( "Couldn't start mainloop.\n" );
    }

    for ( ;; ) {
        pa_context_state_t state;

        state = pa_context_get_state ( kntOutput->paContext );

        if ( state == PA_CONTEXT_READY ) {
            break;
        }

        if ( !PA_CONTEXT_IS_GOOD ( state ) ) {
            kanata_error ( "Context initialization failed: %s\n", pa_strerror ( pa_context_errno ( kntOutput->paContext ) ) );
        }

        // Wait until the context is ready
        pa_threaded_mainloop_wait ( kntOutput->paMainLoop );
    }

    if ( ! ( kntOutput->paStream = pa_stream_new ( kntOutput->paContext, kntPulseClientName, &ss, NULL ) ) ) {
        kanata_error ( "Stream creation failed: %s\n", pa_strerror ( pa_context_errno ( kntOutput->paContext ) ) );
    }

    pa_stream_set_state_callback ( kntOutput->paStream, pulseout_streamstatecallback, kntOutput );
    pa_stream_set_write_callback ( kntOutput->paStream, pulseout_streamwritecallback, kntOutput );
    pa_stream_connect_playback ( kntOutput->paStream, NULL, NULL, ( pa_stream_flags_t ) ( PA_STREAM_INTERPOLATE_TIMING | PA_STREAM_ADJUST_LATENCY | PA_STREAM_AUTO_TIMING_UPDATE ), NULL, NULL );

    for ( ;; ) {
        pa_stream_state_t state;

        state = pa_stream_get_state ( kntOutput->paStream );

        if ( state == PA_STREAM_READY ) {
            break;
        }

        if ( !PA_STREAM_IS_GOOD ( state ) ) {
            kanata_error ( "Stream initialization failed: %s\n", pa_strerror ( pa_context_errno ( kntOutput->paContext ) ) );
        }

        // Wait until the stream is ready
        pa_threaded_mainloop_wait ( kntOutput->paMainLoop );
    }

    pa_threaded_mainloop_unlock ( kntOutput->paMainLoop );
    return kntOutput;
}

void pulseout_free ( KntPulseOutput* kntOutput )
{
//     pa_operation* o;
//     if ( ! ( o = pa_context_drain ( kntOutput->paContext, knt_pulseout_contextnotifycallback, kntOutput ) ) ) {
//
//     } else {
//         pa_operation_unref ( o );
//     }
    pa_threaded_mainloop_stop ( kntOutput->paMainLoop );
    pa_stream_unref ( kntOutput->paStream );
    pa_context_disconnect ( kntOutput->paContext );
    pa_context_unref ( kntOutput->paContext );
    pa_threaded_mainloop_free ( kntOutput->paMainLoop );
    free ( kntOutput );
}

void pulseout_flush ( KntPulseOutput* kntOutput )
{
    pa_threaded_mainloop_lock ( kntOutput->paMainLoop );
//     pa_operation* o = pa_stream_flush ( kntOutput->paStream, NULL, NULL );
//     while ( pa_operation_get_state ( o ) == PA_OPERATION_RUNNING ) {
//         pa_threaded_mainloop_wait ( kntOutput->paMainLoop );
//     }
//     pa_operation_unref ( o );
    pa_threaded_mainloop_unlock ( kntOutput->paMainLoop );
}

void pulseout_playbuffer ( KntPulseOutput* kntOutput, KanataBuffer* kanataBuffer )
{
    const uint8_t* data = kanataBuffer->buffer;
    size_t length = kanataBuffer->size;
    pa_threaded_mainloop_lock ( kntOutput->paMainLoop );

    while ( length > 0 ) {
        size_t l;
        int r;

        while ( ! ( l = pa_stream_writable_size ( kntOutput->paStream ) ) ) {
            pa_threaded_mainloop_wait ( kntOutput->paMainLoop );
        }

        if ( l > length ) {
            l = length;
        }
        // TODO Optimize by writing in the buffer delivered by pa_stream_begin_write

        r = pa_stream_write ( kntOutput->paStream, data, l, NULL, 0LL, PA_SEEK_RELATIVE );

        if ( r < 0 ) {
            kanata_error ( "Error writing output stream.\n" );
        }

        data = data + l;
        length -= l;
    }

    pa_threaded_mainloop_unlock ( kntOutput->paMainLoop );
}

void pulseout_contextnotifycallback ( pa_context* context, void* userdata )
{
    KntPulseOutput* kntOutput = ( KntPulseOutput* ) userdata;
    pa_context_disconnect ( kntOutput->paContext );
}

void pulseout_contextstatecallback ( pa_context* context, void* userdata )
{
    KntPulseOutput* kntOutput = ( KntPulseOutput* ) userdata;
    switch ( pa_context_get_state ( context ) ) {
    case PA_CONTEXT_READY:
    case PA_CONTEXT_TERMINATED:
    case PA_CONTEXT_FAILED:
        pa_threaded_mainloop_signal ( kntOutput->paMainLoop, 0 );
        break;

    case PA_CONTEXT_UNCONNECTED:
    case PA_CONTEXT_CONNECTING:
    case PA_CONTEXT_AUTHORIZING:
    case PA_CONTEXT_SETTING_NAME:
        break;
    }
}

void pulseout_streamstatecallback ( pa_stream* p, void* userdata )
{
    KntPulseOutput* kntOutput = ( KntPulseOutput* ) userdata;
    switch ( pa_stream_get_state ( p ) ) {
    case PA_STREAM_READY:
    case PA_STREAM_FAILED:
    case PA_STREAM_TERMINATED:
        pa_threaded_mainloop_signal ( kntOutput->paMainLoop, 0 );
        break;

    case PA_STREAM_UNCONNECTED:
    case PA_STREAM_CREATING:
        break;
    }
}

void pulseout_streamwritecallback ( pa_stream* p, size_t nbytes, void* userdata )
{
    KntPulseOutput* kntOutput = ( KntPulseOutput* ) userdata;
    pa_threaded_mainloop_signal ( kntOutput->paMainLoop, 0 );
}

