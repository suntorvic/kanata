#ifndef KANATA_H
#define KANATA_H

#include <signal.h>
#include <pthread.h>
#include "kanata_utils.h"
#include "kanata_plugin.h"

struct KanataOutput;
struct KanataFile;
struct KanataPluginBase;
struct KanataTrack;
struct KanataPlayer;

typedef struct KanataCurrentInterface {
    KanataInterface* interface;
    pthread_t thread;
} KanataCurrentInterface;

typedef struct KanataContext {
    KanataLinkedList pluginList;
    KanataLinkedList outputs;
    KanataLinkedList interfaces;
    KanataCurrentInterface currentInterface;
    struct KanataPlayer* player;
    pthread_t playerThread;
    uint8_t continueLoop;
    struct sigaction sigaction_int;
    KanataOpts opts;
    pthread_mutex_t mutex;
} KanataContext;

KanataContext* kanata_create();

void kanata_set_opts ( KanataContext* kntctx, const int argc, char** argv );

int kanata_run ( KanataContext* kntctx );

void* kanata_run_interface ( void* kntctx_voidptr );

void kanata_free ( KanataContext* kntctx );

void kanata_signal_int ( int s );

#endif // KANATA_H

