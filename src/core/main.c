#include <stdio.h>
#include <libavformat/avformat.h>

#include "kanata.h"

int main ( int argc, char** argv )
{
    av_register_all();
    KanataContext* kntctx = kanata_create();
    kanata_set_opts ( kntctx, argc, argv );
    int ret = kanata_run ( kntctx );
    kanata_free ( kntctx );
    puts("Exiting Kanata...");
    return ret;
}
