#ifndef METADATA_H
#define METADATA_H

#include "kanata_utils.h"

struct KanataFile;

const char *kanata_get_tag( struct KanataFile* kanataFile, const char* tag );

#endif /* METADATA_H */