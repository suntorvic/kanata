#include "kanata_plugin.h"
#include "kanata.h"
#include "kanata_utils.h"
#include "plugin_management.h"
#include "metadata.h"
#include "mediaplayer.h"
#include <pthread.h>

void kanata_register_interface ( KanataContext* kntctx, KanataInterface* kntInterface )
{
    kntInterface->plugin = currentPlugin;
    kanata_linkedlist_append ( &kntctx->interfaces, kntInterface );
}

void kanata_register_output ( KanataContext* kntctx, KanataOutput* kntOutput )
{
    kntOutput->plugin = currentPlugin;
    kanata_linkedlist_append ( &kntctx->outputs, kntOutput );
}

void kanata_context_lock ( KanataContext* kntctx )
{
    pthread_mutex_lock ( &kntctx->mutex );
}

void kanata_context_unlock ( KanataContext* kntctx )
{
    pthread_mutex_unlock ( &kntctx->mutex );
}

KanataPlayer* kanata_context_player ( KanataContext* kntctx )
{
    return kntctx->player;
}

unsigned char kanata_loop_continue ( KanataContext* kntctx )
{
    return kntctx->continueLoop;
}

const char* kanata_metadata_getf ( KanataTrack* track, const char* format )
{
  KanataFile* file = track->file;
  return kanata_get_tag(file, format);
}


