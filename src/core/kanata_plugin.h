#ifndef KANATA_PLUGIN_H
#define KANATA_PLUGIN_H

#include "kanata_utils.h"

struct KanataContext;
struct KanataPlugin;

typedef void* ( * KanataPluginInitFunc ) ( struct KanataContext* );
typedef void ( * KanataPluginCloseFunc ) ( struct KanataContext*, void** );

typedef int ( * KIRunFunction ) ( struct KanataContext*, void** );

typedef struct KanataInterface {
    struct KanataPlugin* plugin;
    KIRunFunction runner;
} KanataInterface;

struct KanataOutput;

typedef void ( * KOInitFunction ) ( void** );
typedef void ( * KOFlushFunction ) ( void** );
typedef void ( * KOWriteFunction ) ( KanataBuffer*, void** );
typedef void ( * KOCloseFunction ) ( void** );

typedef struct KanataOutput {
    struct KanataPlugin* plugin;
    KOInitFunction init;
    KOCloseFunction close;
    KOFlushFunction flush;
    KOWriteFunction write;
} KanataOutput;

void kanata_register_interface ( struct KanataContext* kntctx, KanataInterface* kntInterface );

void kanata_register_output ( struct KanataContext* kntctx, KanataOutput* kntOutput );

void kanata_context_lock ( struct KanataContext* kntctx );

void kanata_context_unlock ( struct KanataContext* kntctx );

KanataPlayer* kanata_context_player ( struct KanataContext* kntctx );

unsigned char kanata_loop_continue ( struct KanataContext* kntctx );

const char* kanata_metadata_getf(KanataTrack* track, const char* format);

#endif // KANATA_PLUGIN_H
