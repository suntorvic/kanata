#include "metadata.h"
#include "mediaplayer.h"

const char* kanata_get_tag ( KanataFile* kanataFile, const char* tag )
{
    AVDictionaryEntry* key = av_dict_get ( kanataFile->formatCtx->streams[kanataFile->audioStream]->metadata, tag, NULL, 0 );
    if (key == NULL)
      key = av_dict_get ( kanataFile->formatCtx->metadata, tag, NULL, 0 );
    return key->value;
}
