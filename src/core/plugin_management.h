#ifndef PLUGIN_H
#define PLUGIN_H

#include "kanata.h"
#include "kanata_utils.h"
#include "kanata_plugin.h"

typedef struct KanataPlugin {
    void* moduleHandle;
    KanataPluginInitFunc initFunc;
    KanataPluginCloseFunc closeFunc;
    char* filename;
    char* fullpath;
    char* pluginName;
    void* userdata;
} KanataPlugin;

typedef struct KanataPluginBase {
    KanataLinkedList pluginList;
} KanataPluginBase;

unsigned int kanata_plugins_load ( KanataContext* kntctx );

unsigned int kanata_plugins_loaddir ( KanataLinkedList* kanataPluginLinkedList, const char* directory );

void kanata_plugins_unload ( KanataContext* kntctx );

KanataPlugin* kanata_plugin_load ( const char* filepath );

void kanata_plugin_unload ( KanataContext* kntctx, KanataPlugin* kanataPlugin );

KanataPlugin* currentPlugin;

void kanata_plugins_init ( KanataContext* kntctx );

#endif // PLUGIN_H
