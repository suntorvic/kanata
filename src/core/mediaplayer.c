#include "mediaplayer.h"
#include "plugin_management.h"

#include <libavutil/avutil.h>
#include <libavutil/opt.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

KanataPlayer* kanata_player_create ( )
{
    KanataPlayer* player = ( KanataPlayer* ) malloc ( sizeof ( KanataPlayer ) );
    player->status = KNT_STATUS_STOP;
    kanata_linkedlist_init ( &player->playlists );
    KanataPlaylist* defaultplaylist = kanata_playlist_create ( "Default" );
    player->currentPlaylist = 0;
    player->currentTrack = 0;
    kanata_linkedlist_append ( &player->playlists, defaultplaylist );
    kanata_linkedlist_makearray ( &player->playlists );
    return player;
}

void kanata_player_free ( KanataPlayer* player )
{
    KanataLinkedListElem* cur;
    for ( cur = player->playlists.start; cur != NULL; cur = cur->next ) {
        kanata_playlist_free ( cur->ptr );
    }
    kanata_linkedlist_empty ( &player->playlists );
    free ( player );
}

void* kanata_run_player ( void* kntctx_voidptr )
{
    KanataContext* kntctx = ( KanataContext* ) kntctx_voidptr;
    kanata_context_lock ( kntctx );
    if ( kntctx->outputs.size > 0 ) {
        kntctx->player->output = ( KanataOutput* ) kntctx->outputs.p[0];
    } else {
        kntctx->player->output = NULL;
        puts ( "No output found." );
    }

    ( kntctx->player->output->init ) ( &kntctx->player->output->plugin->userdata );
    kanata_context_unlock ( kntctx );
    unsigned char continueLoop;
    PlayerStatus previous_status;
    KanataBuffer* buf = kanata_buffer_create();
    // Quits if there's not output
    for ( ; kntctx->player->output; ) {
        // Getting values from the mutex...
        kanata_context_lock ( kntctx );
        KanataPlayer* player = kntctx->player;
        KanataPlaylist* currentPlaylist = ( KanataPlaylist* ) player->playlists.p[player->currentPlaylist];
        KanataTrack* currentTrack = NULL;
        if ( currentPlaylist && currentPlaylist->tracks.start != NULL && player->currentTrack < currentPlaylist->tracks.size ) {
            currentTrack = ( KanataTrack* ) currentPlaylist->tracks.p[player->currentTrack];
            if ( previous_status == KNT_STATUS_STOP ) {
                av_seek_frame ( currentTrack->file->formatCtx, currentTrack->file->audioStream, 0, AVSEEK_FLAG_BACKWARD );
            }
        }
        continueLoop = kntctx->continueLoop;
        KanataBuffer* buf2 = NULL;
        if ( player->status == KNT_STATUS_STOP && previous_status != KNT_STATUS_STOP ) {
            av_seek_frame ( currentTrack->file->formatCtx, currentTrack->file->audioStream, 0, AVSEEK_FLAG_BACKWARD );
        } else if ( player->status == KNT_STATUS_PLAY ) {
            if ( currentPlaylist->tracks.start == NULL ) {
                player->status == KNT_STATUS_STOP;
            } else if ( currentTrack ) {
                buf2 = kanata_file_nextPacket ( currentTrack->file, buf );
                if ( !buf2 ) {
                    player->currentTrack++;
                    av_seek_frame ( currentTrack->file->formatCtx, currentTrack->file->audioStream, 0, AVSEEK_FLAG_BACKWARD );
                    if ( player->currentTrack >= currentPlaylist->tracks.size ) {
                        player->status = KNT_STATUS_STOP;
                    } else {
                        KanataTrack* newTrack = currentPlaylist->tracks.p[player->currentTrack];
                        av_seek_frame ( newTrack->file->formatCtx, currentTrack->file->audioStream, 0, AVSEEK_FLAG_BACKWARD );
                    }
                }
            }
        }
        usleep(100); // reduces CPU usage, there's probably a better way.
    unlock_and_continue_loop:
        kanata_context_unlock ( kntctx );
        if ( !continueLoop ) {
            break;
        }
        // Player loop actions

        if ( buf2 && player->status == KNT_STATUS_PLAY ) {
            kanata_context_lock ( kntctx );
            KOWriteFunction write = player->output->write;
            void** userdata = &player->output->plugin->userdata;
            kanata_context_unlock ( kntctx );
            ( * write ) ( buf2, userdata );
            buf = buf2;
        }
        kanata_context_lock ( kntctx );
        previous_status = player->status;
        kanata_context_unlock ( kntctx );
    }
    kanata_buffer_free ( buf );
    kanata_context_lock ( kntctx );
//     ( kntctx->player->output->flush ) ( &kntctx->player->output->plugin->userdata );
    ( kntctx->player->output->close ) ( &kntctx->player->output->plugin->userdata );
    kanata_context_unlock ( kntctx );
    return 0;
}

KanataFile* kanata_file_new ( const char* filename )
{
    KanataFile* kanataFile = ( KanataFile* ) malloc ( sizeof ( KanataFile ) );
    int l = strlen ( filename ) + 1;
    char* filename1 = ( char* ) malloc ( sizeof ( char ) * l );
    strncpy ( filename1, filename, l );
    kanataFile->filename = filename1;

    // TODO Move all of this to a file reader object
    kanataFile->formatCtx = NULL;
    kanataFile->options = NULL;
    if ( avformat_open_input ( & ( kanataFile->formatCtx ), ( const char* ) kanataFile->filename, NULL, NULL ) != 0 ) {
        kanata_error ( "Couldn't open file %s\n", kanataFile->filename );
    }

    // Retrieve stream information
//     for (int i = 0; i <kanataFile->formatCtx->nb_streams; i++)
//          opts[i] = filter_codec_opts(codec_opts, s->streams[i]->codec->codec_id,kanataFile->formatCtx,kanataFile->formatCtx->streams[i], NULL);
    if ( avformat_find_stream_info ( kanataFile->formatCtx, NULL ) < 0 ) {
        kanata_error ( "Couldn't find stream information.\n" );
    }

    kanataFile->audioStream = av_find_best_stream ( kanataFile->formatCtx, AVMEDIA_TYPE_AUDIO, -1, -1, &kanataFile->codec, 0 );
    if ( kanataFile->audioStream == -1 ) {
        kanata_error ( "No audio stream found.\n" );
    }

    // Get a pointer to the codec context for the video stream
    kanataFile->codecCtx = kanataFile->formatCtx->streams[kanataFile->audioStream]->codec;

    kanataFile->sample_fmt =kanataFile->codecCtx->sample_fmt;

    if ( kanataFile->codec == NULL ) {
        kanata_error ( "Unsupported codec.\n" );
    }

    kanataFile->codecCtxOrig =kanataFile->codecCtx;
    // Copy context
    kanataFile->codecCtx = avcodec_alloc_context3 ( kanataFile->codec );
    if ( avcodec_copy_context ( kanataFile->codecCtx,kanataFile->codecCtxOrig ) != 0 ) {
        kanata_error ( "Couldn't copy codec context.\n" );
    }
    // Open codec
    if ( avcodec_open2 ( kanataFile->codecCtx,kanataFile->codec, &kanataFile->options ) < 0 ) {
        kanata_error ( "Couldn't open codec.\n" );
    }
    kanataFile->frame = av_frame_alloc();

    // Target sample format
    kanataFile->sample_fmt = AV_SAMPLE_FMT_S16;

    // Set up SWR context once you've got codec information
    kanataFile->swr = swr_alloc();
    av_opt_set_int ( ( void* ) kanataFile->swr, "in_channel_layout", kanataFile->codecCtx->channel_layout, 0 );
    av_opt_set_int ( ( void* ) kanataFile->swr, "out_channel_layout",kanataFile->codecCtx->channel_layout,  0 );
    av_opt_set_int ( ( void* ) kanataFile->swr, "in_sample_rate",    kanataFile->codecCtx->sample_rate, 0 );
    av_opt_set_int ( ( void* ) kanataFile->swr, "out_sample_rate",   kanataFile->codecCtx->sample_rate, 0 );
    av_opt_set_sample_fmt ( ( void* ) kanataFile->swr, "in_sample_fmt", kanataFile->codecCtx->sample_fmt, 0 );
    av_opt_set_sample_fmt ( ( void* ) kanataFile->swr, "out_sample_fmt",kanataFile->sample_fmt,  0 );
    swr_init ( kanataFile->swr );

//     av_dump_format ( kanataFile->formatCtx, 0, kanataFile->filename, 0 );

    return kanataFile;
}

void kanata_file_free ( KanataFile* kanataFile )
{

    avformat_flush ( kanataFile->formatCtx );
    av_frame_free ( &kanataFile->frame );
    avcodec_close ( kanataFile->codecCtx );
    avcodec_free_context ( &kanataFile->codecCtx );
    avformat_close_input ( &kanataFile->formatCtx );
    swr_free ( &kanataFile->swr );

    free ( kanataFile->filename );
    free ( kanataFile );
}

KanataBuffer* kanata_file_nextPacket ( KanataFile* kanataFile, KanataBuffer* kanataBuffer )
{
    KanataBuffer* retBuffer = NULL;
    av_init_packet ( &kanataFile->pkt );
    while ( av_read_frame ( kanataFile->formatCtx, &kanataFile->pkt ) >= 0 ) {
        if ( kanataFile->pkt.stream_index == kanataFile->audioStream ) {
            int hasFrame;
            int r = avcodec_decode_audio4 ( kanataFile->codecCtx, kanataFile->frame, &hasFrame, &kanataFile->pkt );
            const uint8_t** in = ( const uint8_t** ) kanataFile->frame->extended_data;
            int out_count = kanataFile->frame->nb_samples + 256;
            size_t out_size = av_samples_get_buffer_size ( NULL, kanataFile->frame->channels, out_count, kanataFile->sample_fmt, 0 );
            kanata_buffer_fastmalloc ( kanataBuffer, out_size );
            int len = swr_convert ( kanataFile->swr, &kanataBuffer->buffer, out_count, in, kanataFile->frame->nb_samples );
            kanataBuffer->size = len * kanataFile->frame->channels * av_get_bytes_per_sample ( kanataFile->sample_fmt );

            if ( hasFrame ) {
                retBuffer = kanataBuffer;
                av_free_packet ( &kanataFile->pkt );
                break;
            }
        }
        av_free_packet ( &kanataFile->pkt );
    }
    return retBuffer;
}


