#ifndef KANATA_UTILS_H
#define KANATA_UTILS_H

#include <stdlib.h>

void kanata_error ( const char* __restrict __format, ... );

typedef unsigned char uint8_t;

char* kanata_allocate_string(const char* cstring);
char* kanata_allocate_string_length(const char* cstring, size_t length);

typedef struct KanataOpts {
    int argc;
    char** argv;
} KanataOpts;

typedef struct KanataBuffer {
    uint8_t* buffer;
    size_t size;
} KanataBuffer;

KanataBuffer* kanata_buffer_create();

KanataBuffer* kanata_buffer_copycbuffer ( uint8_t* buffer, size_t size );

KanataBuffer* kanata_buffer_fastmalloc ( KanataBuffer* kanataBuffer, size_t size );

void kanata_buffer_free ( KanataBuffer* kanataBuffer );

typedef struct KanataLinkedListElem {
    void* ptr;
    struct KanataLinkedListElem* next;
} KanataLinkedListElem;

typedef struct KanataLinkedList {
    KanataLinkedListElem* start;
    KanataLinkedListElem* end;
    void** p;
    size_t size;
} KanataLinkedList;

KanataLinkedList* kanata_linkedlist_init ( KanataLinkedList* kanataLinkedList );
void kanata_linkedlist_empty ( KanataLinkedList* kanataLinkedList );
size_t kanata_linkedlist_append ( KanataLinkedList* kanataLinkedList, void* elem );
size_t kanata_linkedlist_remove ( KanataLinkedList* kanataLinkedList, void* elem );
// Call makearray to update the .elements array as well as .size, after each operation or group of operations
void kanata_linkedlist_makearray ( KanataLinkedList* kanataLinkedList );

struct KanataFile;
struct KanataOutput;

typedef struct KanataPlaylist {
    char* name;
    KanataLinkedList tracks;
} KanataPlaylist;

typedef struct KanataTrack {
    struct KanataFile* file;
} KanataTrack;

typedef enum PlayerStatus {
    KNT_STATUS_STOP,
    KNT_STATUS_PAUSE,
    KNT_STATUS_PLAY
} PlayerStatus;

typedef struct KanataPlayer {
    size_t currentPlaylist;
    size_t currentTrack;
    KanataLinkedList playlists;
    struct KanataOutput* output;
    PlayerStatus status;
} KanataPlayer;

KanataPlaylist* kanata_playlist_create ( const char* name );

void kanata_playlist_free ( KanataPlaylist* playlist );

KanataTrack* kanata_track_new ( const char* filename );

void kanata_track_free( KanataTrack* track );

void kanata_player_flush( KanataPlayer* player );

#endif // KANATA_UTILS_H
