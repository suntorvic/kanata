#include "plugin_management.h"
#include <stdlib.h>
#include <dlfcn.h>
#include <stdio.h>
#include <limits.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>

unsigned int kanata_plugins_load ( KanataContext* kntctx )
{
    unsigned int n_plugins_loaded = 0;
    // Linked list for retrieving all plugins easily
    kanata_linkedlist_init ( &kntctx->pluginList );

    char* plugin_dir;
    plugin_dir = ( char* ) malloc ( PATH_MAX * sizeof ( char ) );
    char* plugin_filepath = ( char* ) malloc ( PATH_MAX * sizeof ( char ) );

    // Home plugins directory

    char* xdg_data_home = getenv ( "XDG_DATA_HOME" );
    if ( xdg_data_home ) {
        snprintf ( plugin_dir, PATH_MAX, "%s/.local/share/kanata/plugins", xdg_data_home );
    } else {
        char* home = getenv ( "HOME" );
        if ( home ) {
            snprintf ( plugin_dir, PATH_MAX, "%s/.local/share/kanata/plugins", home );
        }
    }

    n_plugins_loaded += kanata_plugins_loaddir ( &kntctx->pluginList, plugin_dir );
    char* xdg_data_dirs = getenv ( "XDG_DATA_DIRS" );
    if ( xdg_data_dirs != NULL ) {
        // Parsing the various paths separated by a colon :
        size_t stringStart, stringEnd, xdg_data_dir_length;
        xdg_data_dir_length = strlen ( xdg_data_dirs );
        for ( stringStart = 0; stringStart < xdg_data_dir_length; stringStart = stringEnd + 1 ) {
            for ( stringEnd = stringStart; stringEnd < xdg_data_dir_length && xdg_data_dirs[stringEnd] != ':'; stringEnd++ ) {
            }
            strncpy ( plugin_dir, xdg_data_dirs + stringStart, stringEnd - stringStart - 1 );
            n_plugins_loaded += kanata_plugins_loaddir ( &kntctx->pluginList, plugin_dir );
        }
    } else {
        n_plugins_loaded += kanata_plugins_loaddir ( &kntctx->pluginList, "/usr/local/share/kanata/plugins" );
        n_plugins_loaded += kanata_plugins_loaddir ( &kntctx->pluginList, "/usr/share/kanata/plugins" );
    }

    free ( plugin_dir );
    free ( plugin_filepath );

    // Make array from linked list
    kanata_linkedlist_makearray ( &kntctx->pluginList );

    return n_plugins_loaded;
}

unsigned int kanata_plugins_loaddir ( KanataLinkedList* kanataPluginLinkedList, const char* directory )
{
    unsigned int n_plugins_loaded = 0;
    DIR* dp;
    struct dirent* ep;

    dp = opendir ( directory );
    if ( dp != NULL ) {
        while ( ep = readdir ( dp ) ) {
            size_t len = strlen ( ep->d_name );

            if ( strcmp ( ep->d_name + ( len-3 ),".so" ) == 0 ) {
                char* fullname = ( char* ) malloc ( PATH_MAX * sizeof ( char ) );
                snprintf ( fullname, PATH_MAX, "%s/%s", directory, ep->d_name );
                KanataPlugin* plugin = kanata_plugin_load ( fullname );
                if ( plugin ) {
                    kanata_linkedlist_append ( kanataPluginLinkedList, plugin );
                }
                free ( fullname );
            }
        }
        ( void ) closedir ( dp );
    }
    return n_plugins_loaded;
}

void kanata_plugins_unload ( KanataContext* kntctx )
{
    if ( kntctx ) {
        KanataLinkedList* pluginList = &kntctx->pluginList;
        for ( KanataLinkedListElem* cur = pluginList->start; cur != NULL; cur = cur->next ) {
            kanata_plugin_unload ( kntctx, ( KanataPlugin* ) ( cur->ptr ) );
        }
        kanata_linkedlist_empty ( pluginList );
    }
}

KanataPlugin* kanata_plugin_load ( const char* filepath )
{
    void* moduleHandle = dlopen ( filepath, RTLD_NOW );
    if ( !moduleHandle ) {
        printf ( "Couldn't load plugin %s\n", filepath );
        return NULL;
    }
    KanataPluginInitFunc initfunc = ( KanataPluginInitFunc ) dlsym ( moduleHandle, "kanata_plugin_init" );

    const char* error;
    if ( ( error = dlerror() ) != NULL )  {
        fprintf ( stderr, "Couldn't load plugin %s: %s\n", filepath, error );
        return NULL;
    }
    dlerror();    /* Clear any existing error */

    KanataPluginCloseFunc closeFunc = ( KanataPluginCloseFunc ) dlsym ( moduleHandle, "kanata_plugin_close" );

    if ( ( error = dlerror() ) != NULL )  {
        closeFunc = NULL;
    }

    dlerror();

    KanataPlugin* kanataPlugin = ( KanataPlugin* ) malloc ( sizeof ( KanataPlugin ) );
    kanataPlugin->moduleHandle = moduleHandle;
    kanataPlugin->initFunc = initfunc;
    kanataPlugin->closeFunc = closeFunc;
    int len = strlen ( filepath );
    kanataPlugin->filename = NULL;
    kanataPlugin->fullpath = ( char* ) malloc ( ( len + 1 ) * sizeof ( char ) );
    // Copy path and set filename to just the filename
    for ( int i = 0; i < len; i++ ) {
        kanataPlugin->fullpath[i] = filepath[i];
        if ( kanataPlugin->fullpath[i] == '/' ) {
            kanataPlugin->filename = kanataPlugin->fullpath + i + 1;
        }
    }
    printf ( "Loaded plugin %s\n", kanataPlugin->filename );
    return kanataPlugin;
}

void kanata_plugin_unload ( KanataContext* kntctx, KanataPlugin* kanataPlugin )
{
    if ( kanataPlugin->closeFunc ) {
        ( kanataPlugin->closeFunc ) ( kntctx, kanataPlugin->userdata );
    }
    dlclose ( kanataPlugin->moduleHandle );
    free ( kanataPlugin->fullpath );
    free ( kanataPlugin );
}

void kanata_plugins_init ( KanataContext* kntctx )
{
    KanataLinkedList* pluginList = &kntctx->pluginList;
    for ( KanataLinkedListElem* cur = pluginList->start; cur != NULL; cur=cur->next ) {
        currentPlugin = ( KanataPlugin* ) cur->ptr;
        currentPlugin->userdata = ( currentPlugin->initFunc ) ( kntctx );
    }
}

