#include "kanata_utils.h"
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#include "mediaplayer.h"
#include "plugin_management.h"

void kanata_error ( const char* __restrict __format, ... )
{
    int size = 0;
    char* p = NULL;
    va_list ap;

    /* Determine required size */

    va_start ( ap, __format );
    size = vsnprintf ( p, size, __format, ap );
    va_end ( ap );

    if ( size < 0 ) {
        return NULL;
    }

    size++;             /* For '\0' */
    p = malloc ( size );
    if ( p == NULL ) {
        return NULL;
    }

    va_start ( ap, __format );
    size = vsnprintf ( p, size, __format, ap );
    if ( size < 0 ) {
        free ( p );
        return NULL;
    }
    va_end ( ap );

    fprintf ( stderr, p );
    exit ( 1 );
}

char* kanata_allocate_string ( const char* cstring )
{
    size_t len = strlen ( cstring ) + 1;
    char* s = ( char* ) malloc ( sizeof ( char ) * len );
    strncpy ( s, cstring, len );
    return s;
}

char* kanata_allocate_string_length ( const char* cstring, size_t length )
{
    char* s = ( char* ) malloc ( sizeof ( char ) * ( length + 1 ) );
    strncpy ( s, cstring, length );
    s[length] = 0;
    return s;
}

KanataBuffer* kanata_buffer_create()
{
    KanataBuffer* kanataBuffer = ( KanataBuffer* ) malloc ( sizeof ( KanataBuffer ) );
    kanataBuffer->buffer = NULL;
    kanataBuffer->size = 0;
    return kanataBuffer;
}

KanataBuffer* kanata_buffer_copycbuffer ( uint8_t* buffer, size_t size )
{
    KanataBuffer* kanataBuffer = ( KanataBuffer* ) malloc ( sizeof ( KanataBuffer ) );
    kanataBuffer->buffer = buffer;
    kanataBuffer->size = size;
    return kanataBuffer;
}

KanataBuffer* kanata_buffer_fastmalloc ( KanataBuffer* kanataBuffer, size_t size )
{
    if ( kanataBuffer->size >= size ) {
        return kanataBuffer;
    }
    if ( kanataBuffer->buffer ) {
        free ( kanataBuffer->buffer );
    }
    kanataBuffer->buffer = ( uint8_t* ) malloc ( size * sizeof ( uint8_t ) );
    kanataBuffer->size = size;
}

void kanata_buffer_free ( KanataBuffer* kanataBuffer )
{
    if ( kanataBuffer->buffer ) {
        free ( kanataBuffer->buffer );
    }
    free ( kanataBuffer );
}

KanataLinkedList* kanata_linkedlist_init ( KanataLinkedList* kanataLinkedList )
{
    KanataLinkedList* target;
    if ( kanataLinkedList == NULL ) {
        target = ( KanataLinkedList* ) malloc ( sizeof ( KanataLinkedList ) );
    } else {
        target = kanataLinkedList;
    }
    target->start = NULL;
    target->end = NULL;
    target->p = NULL;
    target->size = 0;
    return target;
}

size_t kanata_linkedlist_append ( KanataLinkedList* kanataLinkedList, void* elem )
{
    KanataLinkedListElem* listelem = ( KanataLinkedListElem* ) malloc ( sizeof ( KanataLinkedListElem ) );
    listelem->ptr = elem;
    listelem->next = NULL;
    if ( !kanataLinkedList->start ) {
        kanataLinkedList->start = listelem;
        kanataLinkedList->end = listelem;
    } else {
        kanataLinkedList->end->next = listelem;
    }
    kanataLinkedList->end = listelem;
    return 0;
}

size_t kanata_linkedlist_remove ( KanataLinkedList* kanataLinkedList, void* elem )
{
    size_t i = 0;
    KanataLinkedListElem* current;
    KanataLinkedListElem* previous;
    KanataLinkedListElem* next;
    previous = NULL;
    // Search for the element
    for ( current = kanataLinkedList->start; current != NULL; current = next, i++ ) {
        next = current->next;
        if ( current->ptr == elem ) {
            break;
        }
        previous = current;
    }
    free ( current );
    if ( previous ) {
        previous->next = next;
    }
    else {
      kanataLinkedList->start = next;
    }
    return i;
}

void kanata_linkedlist_makearray ( KanataLinkedList* kanataLinkedList )
{
    if ( !kanataLinkedList->start ) {
        kanataLinkedList->size = 0;
        return;
    }
    KanataLinkedListElem* current;
    size_t i;
    for ( current = kanataLinkedList->start, i = 0; current != NULL; current = current->next, i++ ) {
    }
    kanataLinkedList->size = i;
    if ( !kanataLinkedList->p ) {
        kanataLinkedList->p = ( void** ) malloc ( kanataLinkedList->size * sizeof ( void* ) );
    } else {
        kanataLinkedList->p = ( void** ) realloc ( kanataLinkedList->p, kanataLinkedList->size * sizeof ( void* ) );
    }
    for ( current = kanataLinkedList->start, i = 0; current != NULL; current = current->next, i++ ) {
        kanataLinkedList->p[i] = current->ptr;
    }
}

void kanata_linkedlist_empty ( KanataLinkedList* kanataLinkedList )
{
    KanataLinkedListElem* current;
    KanataLinkedListElem* next;
    for ( current = kanataLinkedList->start; current != NULL; current = next ) {
        next = current->next;
        free ( current );
    }
    if ( kanataLinkedList->p != NULL ) {
        free ( kanataLinkedList->p );
        kanataLinkedList->p = NULL;
    }
    kanataLinkedList->start = NULL;
    kanataLinkedList->end = NULL;
    kanataLinkedList->size = 0;
}

KanataPlaylist* kanata_playlist_create ( const char* name )
{
    KanataPlaylist* playlist = ( KanataPlaylist* ) malloc ( sizeof ( KanataPlaylist ) );
    kanata_linkedlist_init ( &playlist->tracks );
    playlist->name = kanata_allocate_string ( name );
    return playlist;
}

void kanata_playlist_free ( KanataPlaylist* playlist )
{
    KanataLinkedListElem* current;
    for ( current = playlist->tracks.start; current != NULL; current = current->next ) {
        kanata_track_free ( ( KanataTrack* ) current->ptr );
    }
    kanata_linkedlist_empty ( &playlist->tracks );
    free ( playlist->name );
    free ( playlist );
}

KanataTrack* kanata_track_new ( const char* filename )
{
    KanataTrack* track = ( KanataTrack* ) malloc ( sizeof ( KanataTrack ) );
    track->file = kanata_file_new ( filename );
    return track;
}

void kanata_track_free ( KanataTrack* track )
{
    kanata_file_free ( track->file );
    free ( track );
}

void kanata_player_flush ( KanataPlayer* player )
{
    ( player->output->flush ) ( &player->output->plugin->userdata );
}

