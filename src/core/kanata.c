#include "kanata.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "plugin_management.h"
#include "mediaplayer.h"

static KanataContext* currentContext;

KanataContext* kanata_create()
{
    KanataContext* kntctx = ( KanataContext* ) malloc ( sizeof ( KanataContext ) );
    currentContext = kntctx;
    sigemptyset ( & ( kntctx->sigaction_int.sa_mask ) );
    kntctx->sigaction_int.sa_flags = 0;
    kntctx->sigaction_int.sa_restorer = NULL;
    kntctx->sigaction_int.sa_handler = kanata_signal_int;
    sigaction ( SIGINT, &kntctx->sigaction_int, NULL );
    kntctx->continueLoop = 1;
    kanata_plugins_load ( kntctx );
    kanata_linkedlist_init ( &kntctx->interfaces );
    kanata_linkedlist_init ( &kntctx->outputs );
    kntctx->player = kanata_player_create();
    pthread_mutex_init ( &kntctx->mutex, PTHREAD_MUTEX_DEFAULT );
    return kntctx;
}

void kanata_free ( KanataContext* kntctx )
{
    pthread_mutex_destroy ( &kntctx->mutex );
    kanata_player_free ( kntctx->player );
    kanata_plugins_unload ( kntctx );
    kanata_linkedlist_empty ( &kntctx->interfaces );
    kanata_linkedlist_empty ( &kntctx->outputs );
    free ( kntctx );
}

void kanata_set_opts ( KanataContext* kntctx, const int argc, char** argv )
{
    kntctx->opts.argc = argc;
    kntctx->opts.argv = argv;
}

int kanata_run ( KanataContext* kntctx )
{
    kanata_context_lock ( kntctx );
    kanata_plugins_init ( kntctx );
    kanata_linkedlist_makearray ( &kntctx->interfaces );
    kanata_linkedlist_makearray ( &kntctx->outputs );
    // TODO Preferences and settings
    if ( kntctx->interfaces.size > 0 ) {
        kntctx->currentInterface.interface = ( KanataInterface* ) kntctx->interfaces.p[0];
    } else {
        puts ( "No interface found. Exiting..." );
        return 0;
    }
    pthread_create ( & ( kntctx->currentInterface.thread ) , NULL, kanata_run_interface, kntctx );
    pthread_create ( & ( kntctx->playerThread ) , NULL, kanata_run_player, kntctx );
    kanata_context_unlock ( kntctx );
    unsigned char continueLoop;
    for ( ; ; ) {
        kanata_context_lock ( kntctx );
        continueLoop = kntctx->continueLoop;
        kanata_context_unlock ( kntctx );
        if ( !continueLoop ) {
            break;
        }
    }
    pthread_join ( kntctx->currentInterface.thread, NULL );
    pthread_join ( kntctx->playerThread, NULL );
    return 0;
}

void kanata_signal_int ( int s )
{
    kanata_context_lock ( currentContext );
    currentContext->continueLoop = 0;
    kanata_context_unlock ( currentContext );
//     kanata_output_flush ( currentContext->currentOutput );
    // TODO fonction d'exit générale ?
}

void* kanata_run_interface ( void* kntctx_voidptr )
{
    KanataContext* kntctx = ( KanataContext* ) kntctx_voidptr;
    kanata_context_lock ( kntctx );
    void** userdata = &kntctx->currentInterface.interface->plugin->userdata;
    KIRunFunction function = kntctx->currentInterface.interface->runner;
    kanata_context_unlock ( kntctx );
    ( * function ) ( kntctx, userdata );
    kanata_context_lock ( kntctx );
    kntctx->continueLoop = 0;
    kanata_context_unlock ( kntctx );
    return 0;
}


