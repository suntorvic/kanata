#ifndef FILE_H
#define FILE_H

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libswresample/swresample.h>

#include "kanata_utils.h"
#include "kanata_plugin.h"
#include "kanata.h"

/*
 * File struct and functions
 */

typedef struct KanataFile {
    char* filename;
    AVFormatContext* formatCtx;
    AVCodecContext* codecCtx;
    AVCodecContext* codecCtxOrig;
    AVCodec* codec;
    AVFrame* frame;
    AVDictionary* options;
    enum AVSampleFormat sample_fmt;
    SwrContext* swr;
//     uint8_t *audioBuf;
//     unsigned int audioBufSize;
    AVPacket pkt;
    int audioStream;
} KanataFile;

KanataPlayer* kanata_player_create( );

void kanata_player_free ( KanataPlayer* player );

void* kanata_run_player ( void* kntctx_voidptr );

KanataFile* kanata_file_new ( const char* filename );

void kanata_file_free ( KanataFile* kanataFile );

KanataBuffer* kanata_file_nextPacket ( KanataFile* kanataFile, KanataBuffer* kanataBuffer );

#endif // FILE_H
