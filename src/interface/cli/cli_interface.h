#ifndef CLI_INTERFACE_H
#define CLI_INTERFACE_H

#include "kanata_plugin.h"

typedef struct pcword {
    char* p;
    unsigned int l;
} pcword;

void* kanata_plugin_init ( struct KanataContext* kntctx );

KanataInterface kcli;

int kcli_run ( struct KanataContext* kntctx, void** userdata );

#endif
