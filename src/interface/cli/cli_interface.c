#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "cli_interface.h"
#include <wordexp.h>
#include <unistd.h>

void* kanata_plugin_init ( struct KanataContext* kntctx )
{
    kcli.runner = &kcli_run;
    kanata_register_interface ( kntctx, &kcli );
    return NULL;
}

int kcli_run ( struct KanataContext* kntctx, void** userdata )
{
    size_t linesize;
    unsigned int nwords;
    wordexp_t words;
    for ( ; ; ) {
        kanata_context_lock ( kntctx );
        if ( !kanata_loop_continue ( kntctx ) ) {
            kanata_context_unlock ( kntctx );
            break;
        }
        kanata_context_unlock ( kntctx );
        char* line = readline ( ">> " );
        if ( line == NULL ) {
            putc ( '\n', stdout );
            break;
        }
        wordexp ( line, &words, 0 );
        if ( words.we_wordc == 1 && strcmp ( words.we_wordv[0], "exit" ) == 0 ) {
            free ( line );
            wordfree ( &words );
            break;
        }
        if ( line && *line ) {
            add_history ( line );
        }
        if ( words.we_wordc > 0  && strcmp ( words.we_wordv[0], "play" ) == 0 ) {
            kanata_context_lock ( kntctx );
            KanataPlayer* player = kanata_context_player ( kntctx );
            if ( words.we_wordc > 1 ) {
                KanataPlaylist* defaultPlaylist = NULL;
                for ( int i = 0; i < player->playlists.size; i++ ) {
                    KanataPlaylist* playlist = ( KanataPlaylist* ) player->playlists.p[i];
                    if ( !strcmp ( playlist->name, "Default" ) ) {
                        defaultPlaylist = playlist;
                        break;
                    }
                }
                if ( !defaultPlaylist ) {
                    defaultPlaylist = kanata_playlist_create ( "Default" );
                    kanata_linkedlist_append ( &player->playlists, defaultPlaylist );
                }
                kanata_linkedlist_makearray ( &player->playlists );

                kanata_linkedlist_empty ( &defaultPlaylist->tracks );
		
                player->currentPlaylist = player->playlists.size - 1;
                for ( size_t i = 1; i < words.we_wordc; i++ ) {
                    kanata_linkedlist_append ( &defaultPlaylist->tracks, kanata_track_new ( words.we_wordv[i] ) );
                }
                player->currentTrack = 0;
                kanata_linkedlist_makearray ( &defaultPlaylist->tracks );
            }
            player->status = KNT_STATUS_PLAY;
            kanata_context_unlock ( kntctx );
        } else if ( words.we_wordc == 1 && strcmp ( words.we_wordv[0], "pause" ) == 0 ) {
            kanata_context_lock ( kntctx );
            KanataPlayer* player = kanata_context_player ( kntctx );
            player->status = KNT_STATUS_PAUSE;
            kanata_player_flush ( player );
            kanata_context_unlock ( kntctx );
        } else if ( words.we_wordc == 1 && strcmp ( words.we_wordv[0], "stop" ) == 0 ) {
            kanata_context_lock ( kntctx );
            KanataPlayer* player = kanata_context_player ( kntctx );
            player->status = KNT_STATUS_PAUSE;
            kanata_player_flush ( player );
            kanata_context_unlock ( kntctx );
        } else if ( words.we_wordc == 1 && strcmp ( words.we_wordv[0], "status" ) == 0 ) {
            kanata_context_lock ( kntctx );
            KanataPlayer* player = kanata_context_player ( kntctx );
            switch ( player->status ) {
            case KNT_STATUS_PAUSE:
                puts ( "Paused" );
                break;
            case KNT_STATUS_PLAY:
                puts ( "Playing" );
                break;
            case KNT_STATUS_STOP:
                puts ( "Stopped" );
                break;
            }
            kanata_player_flush ( player );
            kanata_context_unlock ( kntctx );
        } else if ( words.we_wordc > 1 && strcmp ( words.we_wordv[0], "add" ) == 0 ) {
            kanata_context_lock ( kntctx );
            KanataPlayer* player = kanata_context_player ( kntctx );
            KanataPlaylist* currentPlaylist = player->playlists.p[player->currentPlaylist];
            for ( size_t i = 1; i < words.we_wordc; i++ ) {
                kanata_linkedlist_append ( &currentPlaylist->tracks, kanata_track_new ( words.we_wordv[i] ) );
            }
            kanata_linkedlist_makearray ( &currentPlaylist->tracks );
            kanata_context_unlock ( kntctx );
        } else if ( words.we_wordc > 1 && !strcmp ( words.we_wordv[0], "prev" ) ) {
            kanata_context_lock ( kntctx );
            KanataPlayer* player = kanata_context_player ( kntctx );
            KanataPlaylist* currentPlaylist = player->playlists.p[player->currentPlaylist];
            if ( player->currentTrack > 0 ) {
                player->currentTrack--;
            }
            kanata_context_unlock ( kntctx );
        } else if ( words.we_wordc > 1 && !strcmp ( words.we_wordv[0], "next" ) ) {
            kanata_context_lock ( kntctx );
            KanataPlayer* player = kanata_context_player ( kntctx );
            KanataPlaylist* currentPlaylist = player->playlists.p[player->currentPlaylist];
            if ( player->currentTrack < currentPlaylist->tracks.size - 1 ) {
                player->currentTrack++;
            }
            kanata_context_unlock ( kntctx );
        } else if ( words.we_wordc > 0 && strcmp ( words.we_wordv[0], "track" ) == 0 ) {
            kanata_context_lock ( kntctx );
            KanataPlayer* player = kanata_context_player ( kntctx );
            kanata_context_unlock ( kntctx );
        } else if ( words.we_wordc > 0 && ( strcmp ( words.we_wordv[0], "playlist" ) == 0 || strcmp ( words.we_wordv[0], "pl" ) == 0 ) ) {
            kanata_context_lock ( kntctx );
            KanataPlayer* player = kanata_context_player ( kntctx );
            if ( strcmp ( words.we_wordv[1], "list" ) == 0 ) {
                for ( size_t i = 0; i < player->playlists.size; i++ ) {
                    KanataPlaylist* playlist = ( KanataPlaylist* ) player->playlists.p[i];
                    char selected = ' ';
                    if ( i == player->currentPlaylist ) {
                        selected = '*';
                    }
                    printf ( "%c %s\n", selected, playlist->name );
                }
            } else if ( strcmp ( words.we_wordv[1], "show" ) == 0 ) {
                KanataPlaylist* currentPlaylist = player->playlists.p[player->currentPlaylist];
                if ( currentPlaylist )
                    for ( size_t i = 0; i < currentPlaylist->tracks.size; i++ ) {
                        KanataTrack* track = ( KanataTrack* ) currentPlaylist->tracks.p[i];
                        char selected = ' ';
                        if ( i == player->currentTrack ) {
                            selected = '*';
                        }
                        const char* title = kanata_metadata_getf ( track, "TITLE" );
                        printf ( "%c %2.d. %s\n", selected, i + 1, title ? title : "?" );
                    }
            }
            kanata_context_unlock ( kntctx );
        } else if ( words.we_wordc == 2 && strcmp ( words.we_wordv[0], "cd" ) == 0 ) {
            struct stat s;
            char* path = words.we_wordv[1];
            int err = stat ( path, &s );
            if ( S_ISDIR ( s.st_mode ) ) {
                chdir ( path );
            } else {
                printf ( "%s is not a directory\n", path );
            }
        } else if ( words.we_wordc == 1 && strcmp ( words.we_wordv[0], "pwd" ) == 0 ) {
            char* currentdirectory = ( char* ) malloc ( sizeof ( char ) * PATH_MAX );
            getcwd ( currentdirectory, PATH_MAX );
            puts ( currentdirectory );
            free ( currentdirectory );
        }
        free ( line );
        wordfree ( &words );
    }
    return 0;
}
